#!/bin/sh
xrandr --output HDMI-2 --primary
xrandr --auto --output DVI-D-1 --left-of HDMI-2
xset s off -dpms
xsetroot -solid "#b8b09a"
