/* user and group to drop privileges to */
static const char *user  = "nobody";
static const char *group = "nobody";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "#424242",     /* after initialization */
	[INPUT] =  "#0486c7",   /* during input */
	[FAILED] = "#b75b57",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;
