link:
	ln -sf $(shell pwd)/scripts ~/scripts
	ln -sf $(shell pwd)/charmap ~/charmap
	mkdir -p ~/.config/fish; ln -sf $(shell pwd)/.config/fish/config.fish ~/.config/fish/config.fish
	mkdir -p ~/.config/sxhkd; ln -sf $(shell pwd)/.config/sxhkd/sxhkdrc ~/.config/sxhkd/sxhkdrc
