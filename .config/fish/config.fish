set -gx PATH /home/jacob/go/bin $PATH
set -gx PATH /home/jacob/.cargo/bin/ $PATH

abbr ls 'ls -lhH'
abbr sl 'ls'
abbr c 'clear'
abbr n 'neofetch'
abbr i 'pacaur -S'
abbr u 'pacaur -Rsn'
abbr car 'cat'
abbr o xdg-open

alias cd..='cd ..'
#alias pacaur='pikaur'
alias vim='nvim'
alias :wq='echo "You aren\'t in vim, Fuckwit"'
alias :q='echo "You aren\'t in vim, Fuckwit"'
alias :w='echo "You aren\'t in vim, Fuckwit"'

umask 033

if status --is-login
    sh scripts/welcome
end

fish_vi_key_bindings
export _JAVA_AWT_WM_NONREPARENTING=1
